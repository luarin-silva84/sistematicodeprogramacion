﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistematicoCuatroDeProgramacionII
{
    public partial class FrmUsuario : Form
    {
        private DataTable tablaUsuario;
        private DataSet dsUsurio;
        private BindingSource bsUsuario;
        private DataRow drUsuario;
        private object int32;

        public DataRow DrUsuario
        {
            

            set
            {
                drUsuario = value;
                txtId.Text = drUsuario["Id"].ToString();
                txtNombre.Text = drUsuario["Nombre"].ToString();
                masktxtContra.Text = drUsuario["Contraseña"].ToString();
                masktxtConfcontra.Text = drUsuario["Confirmar Contraseña"].ToString();
                txtCorreo.Text = drUsuario["Correo"].ToString();
                txtConfiCor.Text = drUsuario["Confirmar Correo"].ToString();
                maskTelef.Text = int32.Parse(drUsuario["Telefono"].ToString());
                maskConfTelef.Text = int32.Parse(drUsuario["Confirmar Telefono"].ToString());
                maskCantFallida.Text = int32.Parse(drUsuario["Cantidad de Acceso fallidos"].ToString());
            }
        }

        public DataTable TablaUsuario
        {
            

            set
            {
                tablaUsuario = value;
            }
        }

        public DataSet DsUsurio
        {

            set
            {
                dsUsurio = value;
            }
        }

        public FrmUsuario()
        {
            InitializeComponent();
            bsUsuario = new BindingSource();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            string id, nombre, correo, contraseña ;
            int telefono, accionesFallidas;

            id = txtId.Text;
            nombre = txtNombre.Text;
            correo = txtCorreo.Text;
            contraseña = masktxtContra.Text;
            telefono = int32.Parse(maskTelef.Text);
            accionesFallidas = int32.Parse(maskCantFallida);

            if (drUsuario != null)
            {
                DataRow drnew = tablaUsuario.NewRow();

                int index = tablaUsuario.Rows.IndexOf(drUsuario);

            }
        }

        private void FrmUsuario_Load(object sender, EventArgs e)
        {
            bsUsuario.DataSource = dsUsuario;
            bsUsuario.DataMember = dsUsurio.Tables["Usuario"].TableName;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistematicoCuatroDeProgramacionII.entities
{
    class Usuario
    {
        private int id;
        private string nombreUsuario;
        private string contraseña;
        private string confContraseña;
        private string correo;
        private string confCorreo;
        private int telefono;
        private int confTelefono;
        private int cantidad_acc_fallido;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Contraseña
        {
            get
            {
                return contraseña;
            }

            set
            {
                contraseña = value;
            }
        }

        public string ConfContraseña
        {
            get
            {
                return confContraseña;
            }

            set
            {
                confContraseña = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string ConfCorreo
        {
            get
            {
                return confCorreo;
            }

            set
            {
                confCorreo = value;
            }
        }

        public int Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public int ConfTelefono
        {
            get
            {
                return confTelefono;
            }

            set
            {
                confTelefono = value;
            }
        }

        public int Cantidad_acc_fallido
        {
            get
            {
                return cantidad_acc_fallido;
            }

            set
            {
                cantidad_acc_fallido = value;
            }
        }

        public string NombreUsuario
        {
            get
            {
                return nombreUsuario;
            }

            set
            {
                nombreUsuario = value;
            }
        }

        public Usuario(int id, string nombreUsuario, string contraseña, string confContraseña, string correo, string confCorreo, int telefono, int confTelefono, int cantidad_acc_fallido)
        {
            this.Id = id;
            this.NombreUsuario = nombreUsuario;
            this.Contraseña = contraseña;
            this.ConfContraseña = confContraseña;
            this.Correo = correo;
            this.ConfCorreo = confCorreo;
            this.Telefono = telefono;
            this.ConfTelefono = confTelefono;
            this.Cantidad_acc_fallido = cantidad_acc_fallido;
        }
    }
}

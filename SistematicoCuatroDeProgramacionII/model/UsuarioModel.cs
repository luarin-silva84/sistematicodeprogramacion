﻿using SistematicoCuatroDeProgramacionII.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistematicoCuatroDeProgramacionII.model
{
    class UsuarioModel
    {
        private static List<Usuario> ListUsuario = new List<Usuario>();


        public static List<Usuario> GetListUsuario()
        {
            return ListUsuario;
        }

        public static void Populate()
        {
            Usuario[] usuarios =
            {
                new Usuario (001,"Betado01","****","****","betado@gmail.com","betado@gmail.com",77558899,77558899,2)
            };
            ListUsuario = usuarios.ToList();
        }
            
    }
}

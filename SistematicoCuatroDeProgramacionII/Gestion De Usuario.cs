﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistematicoCuatroDeProgramacionII
{
    public partial class Gestion_De_Usuario : Form
    {
        private DataSet dsUsuario;
        private BindingSource bsUsuario;

        public DataSet DsUsuario
        {
            
            set
            {
                dsUsuario = value;
            }
        }

        public BindingSource BsUsuario
        {
           
            set
            {
                bsUsuario = value;
            }
        }

        public Gestion_De_Usuario()
        {
            InitializeComponent();
            bsUsuario = new BindingSource();

        }

        private void Gestion_De_Usuario_Load(object sender, EventArgs e)
        {

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmUsuario usua= new FrmUsuario();
            usua.tablaUsuario = new dsUsuario();

        }
    }
}
